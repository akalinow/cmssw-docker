#!/bin/sh
set  -e

/etc/cvmfs/run-cvmfs.sh
#/etc/eos/run-eos.sh #EOS packeges fo EL9 are not available publicly?

echo "::: Setting up CMS environment..."
source /cvmfs/cms.cern.ch/cmsset_default.sh
source /cvmfs/cms.cern.ch/crab3/crab.sh prod
echo "::: Setting up CMS environment... [done]"

exec "$@"
