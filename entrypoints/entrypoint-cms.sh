#!/bin/sh
set  -e

#/etc/eos/run-eos.sh #EOS packeges fo EL9 are not available publicly?

echo "::: Setting up CMS environment\
 (works only if /cvmfs is mounted on host) ..."
if [ -f /cvmfs/cms.cern.ch/cmsset_default.sh ]; then
    source /cvmfs/cms.cern.ch/cmsset_default.sh
    source /cvmfs/cms.cern.ch/crab3/crab.sh prod
  echo "::: Setting up CMS environment... [done]"
else
  echo "::: Could not set up CMS environment... [ERROR]"
  echo "::: /cvmfs/cms.cern.ch/cmsset_default.sh not found/available"
fi

echo ":::To run jupyter type:"
echo "./opt/scripts/start-jupyter.sh"

exec "$@"
