#!/bin/sh

echo "::: mounting eos/user/a/..."
sudo eosxd -ofsname=home-a
echo "::: [done]"

echo "::: mounting eos/user/k/..."
sudo eosxd -ofsname=home-k
echo "::: [done]"

echo "::: mounting eos/cms/..."
sudo eosxd -ofsname=cms
echo "::: [done]"

echo "Remember to run kinit to access eos."
